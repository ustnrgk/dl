<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ArchiveController
 * @Route("/archive", name="archive")
 * @package AppBundle\Controller
 */
class ArchiveController extends Controller
{
    /**
     * @param $page
     * @Route(
     *     "/all/{page}",
     *     defaults={"page": 1},
     *     requirements={"page": "\d+"},
     *     name="archiveAll"
     * )
     * @Method("GET")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAllAction($page)
    {
        return new JsonResponse(
            array(
                'data' => $this->renderView(
                    'AppBundle:Archive:viewAll.html.twig',
                    array(
                        'page' => $page
                    )
                )
            )
        );
    }

    /**
     * @param $week
     * @Route(
     *     "/{_locale}/week/{week}",
     *     defaults={
     *          "week": 1,
     *          "_locale": "tr"
     *     },
     *     requirements={
     *          "week": "\d+",
     *          "_locale": "tr|en|fr"
     *     },
     *     name="archiveByWeek"
     * )
     * @Method("GET")
     * @return JsonResponse
     */
    public function getByWeekAction($week)
    {
        return $this->get('request')->attributes->count();
        return new JsonResponse(
            array(
                'week' => $week,
                'locale' => $this->get('request')->getLocale()
            )
        );

        return new JsonResponse(
            array(
                'url' => $this->get('router')->generate(
                    'archiveByWeek', array(
                        'week' => 3
                    )
                )
            )
        );

        return new JsonResponse(
            $this->get('router')->match('/archive/en/week/1')
        );
    }
}
