<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\Exception\InvalidArgumentException;

class LuckyController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('', array('name' => $name));
    }

    /**
     * @Route("/lucky/number", name="number")
     */
    public function numberAction()
    {
        $content = array(
            'numbers' => array(
                rand(0, 100),
                rand(0, 100),
                rand(0, 100)
            )
        );

        return new JsonResponse($content);
    }

    /**
     * @param integer $minRange
     * @param integer $maxRange
     * @param integer $count
     * @Route("/lucky/number/{minRange}/{maxRange}/{count}", name="numberWithCount")
     * @throws \Exception
     * @return array
     */
    public function numberWithCountAction($minRange, $maxRange, $count)
    {
        if ($minRange >= $maxRange) {
            throw new \Exception('maxRange must be greater than minRange');
        }

        $range = $maxRange - $minRange;

        if ($count > $range) {
            throw new \Exception(
                'A count variable greater than maxRange, causes an infinite loop.'
            );
        }

        $numbers = array();

        for ($i = 0; $i < $count; $i++) {
            $randomNumber = mt_rand($minRange, $maxRange);

            while (in_array($randomNumber, $numbers)) {
                $randomNumber = mt_rand($minRange, $maxRange);
            }

            $numbers[] = $randomNumber;
        }

        $data = array('numbers' => $numbers);

        return new JsonResponse($data);
    }
}
