<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Country;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;

class CountryController extends Controller
{
    /**
     * @param Request $request
     * @Route("/country/create", name="createCountry")
     * @Method("POST")
     * @throws \Exception
     * @return JsonResponse
     */
    public function createCountryAction(Request $request)
    {
        $name = $request->request->get('name', null);
        $population = $request->request->get('population', null);

        if (! empty($name) && ! empty($population)) {
            $country = new Country();
            $country->setName($name);
            $country->setPopulation($population);
            $em = $this->getDoctrine()->getManager();

            try {
                $em->persist($country);
                $em->flush();
                $country = $this
                    ->getDoctrine()
                    ->getRepository('AppBundle:Country')
                    ->findOneBy(
                        array(
                            'name' => $name
                        )
                    );

                if ($country instanceof Country) {

                    return new JsonResponse(
                        array(
                            'success' => true,
                            'data' => array(
                                'id' => $country->getId(),
                                'name' => $country->getName(),
                                'population' => $country->getPopulation()
                            )
                        )
                    );
                }
            } catch (\Exception $e) {
                return sprintf('%s: %s', $e->getCode(), $e->getMessage());
            }

        } else {
            throw new \Exception('Any arguments can\'t be null');
        }
    }

    /**
     * @param Request $request
     * @param integer $id
     * @Route(
     *     "/country/{id}/update",
     *     name="updateCountry",
     *     requirements={"id": "\d+"}
     * )
     * @Method("POST")
     * @throws \Exception
     * @return JsonResponse
     */
    public function updateCountryAction(Request $request, $id)
    {
        $name = $request->request->get('name', null);
        $population = $request->request->get('population', null);
        
        if (!empty($id) && !empty($name) && !empty($population)) {
            $em = $this->getDoctrine()->getManager();

            /** @var Country $country */
            $country = $em->getRepository('AppBundle:Country')->find($id);

            $country->setName($name);
            $country->setPopulation($population);

            try {
                $em->flush();

                /** @var Country $country */
                $country = $this->getDoctrine()
                    ->getRepository('AppBundle:Country')
                    ->find($id);
                return new JsonResponse(
                    array(
                        'success' => true,
                        'data' => array(
                            'id' => $country->getId(),
                            'name' => $country->getName(),
                            'population' => $country->getPopulation()
                        )
                    )
                );
            } catch (\Exception $e) {
                return sprintf('%s: %s', $e->getCode(), $e->getMessage());
            }
        } else {
            throw new \Exception('Any arguments can\'t be null');
        }
    }

    /**
     * @param integer $id
     * @Route(
     *     "/country/{id}",
     *     name="getCountry",
     *     requirements={"id": "\d+"}
     * )
     * @Method("GET")
     * @throws \Exception
     * @return JsonResponse
     */
    public function getCountryAction($id)
    {
        /** @var Country $country */
        $country = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Country')
            ->find($id);

        return new JsonResponse(
            array(
                'success' => true,
                'data' => array(
                    'id' => $country->getId(),
                    'name' => $country->getName(),
                    'population' => $country->getPopulation()
                )
            ),
            200,
            array(
                'Access-Control-Allow-Origin' => 'http://localhost:8888',
                'Access-Control-Allow-Credentials' => true
            )
        );
    }
}
